package ssm.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.LanguagePropertyType.TOOLTIP_NEXT_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_PREVIOUS_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_REMOVE_SLIDE;
import static ssm.StartupConstants.CSS_CLASS_VERTICAL_TOOLBAR_BUTTON;
import static ssm.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import static ssm.StartupConstants.ICON_NEXT;
import static ssm.StartupConstants.ICON_PREVIOUS;
import static ssm.StartupConstants.ICON_REMOVE_SLIDE;
import static ssm.StartupConstants.PATH_DATA;
import ssm.model.SlideShowModel;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;
import ssm.view.SlideShowMakerView;
import static ssm.StartupConstants.PATH_SLIDE_SHOWS;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import static ssm.file.SlideShowFileManager.JSON_EXT;
import static ssm.file.SlideShowFileManager.SLASH;
import ssm.model.Slide;

/**
 * This class serves as the controller for all file toolbar operations, driving
 * the loading and saving of slide shows, among other things.
 *
 * @author McKilla Gorilla & _____________
 * @co_author Olawale Onigemo
 */
public class FileController {

    // WE WANT TO KEEP TRACK OF WHEN SOMETHING HAS NOT BEEN SAVED
    private boolean saved;

    // THE APP UI
    private SlideShowMakerView ui;

    // THIS GUY KNOWS HOW TO READ AND WRITE SLIDE SHOW DATA
    private SlideShowFileManager slideShowIO;

    private Slide slide;

    private int index;
    private String imagePath;
    private File file;
    private Image slideImage;
    private URL fileURL;
    private ImageView theImage;
    private Object dialog;
    private Label captionLabel;
    private String[] imageFileNames;
    private String slidesImageName;

    /**
     * This default constructor starts the program without a slide show file
     * being edited.
     *
     * @param initSlideShowIO The object that will be reading and writing slide
     * show data.
     */
    public FileController(SlideShowMakerView initUI, SlideShowFileManager initSlideShowIO) {
        // NOTHING YET
        saved = true;
        ui = initUI;
        slideShowIO = initSlideShowIO;
    }

    public void markAsEdited() {
        saved = false;
        ui.updateToolbarControls(saved);
    }

    /**
     * This method starts the process of editing a new slide show. If a pose is
     * already being edited, it will prompt the user to save it first.
     */
    public void handleNewSlideShowRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToMakeNew = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToMakeNew = promptToSave();
            }

            // IF THE USER REALLY WANTS TO MAKE A NEW COURSE
            if (continueToMakeNew) {
                // RESET THE DATA, WHICH SHOULD TRIGGER A RESET OF THE UI
                SlideShowModel slideShow = ui.getSlideShow();

                slideShow.reset();
                saved = false;

                // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
                // THE APPROPRIATE CONTROLS
                ui.updateToolbarControls(saved);

                // TELL THE USER THE SLIDE SHOW HAS BEEN CREATED
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information Dialog");
                alert.setHeaderText(null);
                alert.setContentText("New slide show has been created!!");

                alert.showAndWait();
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            // @todo provide error message
        }
    }

    /**
     * This method lets the user open a slideshow saved to a file. It will also
     * make sure data for the current slideshow is not lost.
     */
    public void handleLoadSlideShowRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToOpen = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToOpen = promptToSave();
            }

            // IF THE USER REALLY WANTS TO OPEN A POSE
            if (continueToOpen) {
                // GO AHEAD AND PROCEED MAKING A NEW POSE
                promptToOpen();
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            //@todo provide error message
        }
    }

    /**
     * This method will save the current slideshow to a file. Note that we
     * already know the name of the file, so we won't need to prompt the user.
     */
    public boolean handleSaveSlideShowRequest() {
        try {
            // GET THE SLIDE SHOW TO SAVE
            SlideShowModel slideShowToSave = ui.getSlideShow();

            // SAVE IT TO A FILE
            slideShowIO.saveSlideShow(slideShowToSave);

            // MARK IT AS SAVED
            saved = true;

            // AND REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
            // THE APPROPRIATE CONTROLS
            ui.updateToolbarControls(saved);

            return true;
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            // @todo
            return false;
        }
    }

    public void handleViewSlideShowRequest() {
        try {
            Stage secondStage;
            Scene secondScene;
            BorderPane secondPane;
            HBox theBox;
            Button previousButton;
            Button nextButton;

            //Get the slide at the first index.
            index = 0;
            slide = ui.getSlideShow().getSlides().get(index);
            imagePath = slide.getImagePath() + SLASH + slide.getImageFileName();
            file = new File(imagePath);

            theBox = new HBox();
            secondStage = new Stage();
            secondPane = new BorderPane();

            previousButton = ui.initChildButton(theBox, ICON_PREVIOUS, TOOLTIP_PREVIOUS_SLIDE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
            nextButton = ui.initChildButton(theBox, ICON_NEXT, TOOLTIP_NEXT_SLIDE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
            previousButton.setDisable(false);
            nextButton.setDisable(false);

            theImage = new ImageView();

            fileURL = file.toURI().toURL();
            slideImage = new Image(fileURL.toExternalForm());

            theImage.setImage(slideImage);

            double scaledWidth = 500;
            double perc = scaledWidth / slideImage.getWidth();
            double scaledHeight = 500;
            theImage.setFitWidth(scaledWidth);
            theImage.setFitHeight(scaledHeight);

            secondPane.setBottom(theBox);//Add buttons for this.
            secondPane.setCenter(theImage);

            // The size of the viewSlideShowView pane.
            secondScene = new Scene(secondPane, 600, 600);

            previousButton.setOnAction(e -> {
                try {
                    //If the user clicks the previous button on the first slide nothing should happen to avoid
                    //index out of bounds. 
                    if (index > -1) {

                        slide = ui.getSlideShow().getSlides().get(index - 1);
                        imagePath = slide.getImagePath() + SLASH + slide.getImageFileName();
                        file = new File(imagePath);
                        fileURL = file.toURI().toURL();
                        slideImage = new Image(fileURL.toExternalForm());
                        theImage.setImage(slideImage);

                        //Update the index 
                        index--;
                    }

                } catch (MalformedURLException ex) {
                    Logger.getLogger(FileController.class.getName()).log(Level.SEVERE, null, ex);
                }

            });

            nextButton.setOnAction(e -> {
                try {
                    if (index != ui.getSlideShow().getSlides().size() - 1) {
                        slide = ui.getSlideShow().getSlides().get(index + 1);
                        imagePath = slide.getImagePath() + SLASH + slide.getImageFileName();
                        file = new File(imagePath);
                        fileURL = file.toURI().toURL();
                        slideImage = new Image(fileURL.toExternalForm());
                        theImage.setImage(slideImage);
                        index++;
                    }

                } catch (MalformedURLException ex) {
                    Logger.getLogger(FileController.class.getName()).log(Level.SEVERE, null, ex);
                }
            });

            secondScene.getStylesheets().add(STYLE_SHEET_UI);
            secondStage.setScene(secondScene);
            secondStage.show();
        } catch (MalformedURLException ex) {
            Logger.getLogger(FileController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void handleViewWebView() {

        //Getting the ImageFileNames into an array of strings. 
        imageFileNames = new String[ui.getSlideShow().getSlides().size()];
        for (int j = 0; j < imageFileNames.length; j++) {
            imageFileNames[j] = "./img/" + ui.getSlideShow().getSlides().get(j).getImageFileName();
        }
        System.out.println(imageFileNames);

        //Set up the image at the beginning of the slideShow to display. 
        String javaScriptArrayName;
        String slidesImageName = "[\"" + imageFileNames[0] + "\"";
        System.out.println(slidesImageName);

        //The beginning of the Javascript array. 
        javaScriptArrayName = "var slideShow = " + "\"";

        //Now to combine the strings by concatenating and sending that string into a 
        for (int j = 0; j < imageFileNames.length; j++) {
            slidesImageName = slidesImageName + ", \"" + imageFileNames[j] + "\"";
        }
        //Set up the ending of the string pattern for javascript format.
        slidesImageName = slidesImageName + "];";
        System.out.println(slidesImageName);

        javaScriptArrayName = "var slideShow = " + slidesImageName;
        System.out.println(javaScriptArrayName);

        String slideShowPath = new String();

        //Get the path for the main project directory. 
        File currentDirectory = new File(new File("").getAbsolutePath());

        //Get the path of the sites folder by getting absolute path while completing the path with a sites string.
        String sitesDirectory = currentDirectory.getAbsolutePath() + "/sites";

        //This will get the path of the index.html file
        String htmlFile = currentDirectory.getAbsolutePath() + "/index.html";
        //This will get the path of the css file.
        String cssFile = currentDirectory.getAbsolutePath() + "/slideshow_style.css";
        //This will get the path of the javascript file
        String jsFile = currentDirectory.getAbsolutePath() + "/Slideshow.js";

        //Get the title of the slideshow
        String slideShowTitle = ui.getSlideShow().getTitle();
        //Get the path of the slideshow.
        slideShowPath = sitesDirectory + "/" + slideShowTitle;

        //For the directories of the  css, js and imgs. 
        String cssDirectory = "css";
        String jsDirectory = "js";
        String imgDirectory = "img";

        //These arethe html/css/js/imgs files that are being copied. 
        File sourceHTMLFile = new File(htmlFile);
        File sourceCSSFile = new File(cssFile);
        File sourceJSFile = new File(jsFile);
        File[] sourceImage = new File[ui.getSlideShow().getSlides().size()];

        //Gets the path of the cssDirectory 
        String cssPath = sitesDirectory + "/" + slideShowTitle + "/" + cssDirectory;
        //Gets the path of the jsDirectory 
        String jsPath = sitesDirectory + "/" + slideShowTitle + "/" + jsDirectory;
        String imgPath = sitesDirectory + "/" + slideShowTitle + "/" + imgDirectory;

        //Get the directory and the path of the json files of the slideshow
        String dataDirectory = currentDirectory.getAbsolutePath() + "/data";
        String jsonPath = dataDirectory + "/slide_shows" + SLASH + slideShowTitle + ".json";

        //This is the location where the copied filed should be placed as well as the name. 
        File destinationHTMLFile = new File(slideShowPath + "/index.html");
        File destinationCSSFile = new File(cssPath + "/" + "slideshow_style.css");
        File destinationJSFile = new File(jsPath + "/" + "SlideShow" + ".js");
        File[] destinationImg = new File[ui.getSlideShow().getSlides().size()];

        //jsonVar = jsonVar.replaceAll("\\\\", "/");
        //System.out.println(slidesTitle);
        String jsonString = "/* To change this license header, choose License Headers in Project Properties.\n"
                + " * To change this template file, choose Tools | Templates\n"
                + " * and open the template in the editor.\n"
                + " */\n"
                + "\n"
                + "\n"
                + "var imageArray = new Array();\n"
                + "var index = 0;\n"
                + "var interval = 1000;\n"
                + "var clickCount = 1;\n"
                + "var timer;\n"
                + "var i = 0;\n"
                + "\n"
                + "\n"
                + "\n"
                + "\n"
                + "/**$(document).ready(function () {\n"
                + " $.getJSON(jsonPath, function (data) {\n"
                + " theTitle = data.title;\n"
                + " theSlides = data.slides;\n"
                + " document.getElementById(\"header\").innerHTML = theTitle;\n"
                + " for (i = 0; i < imageArray.length; i++) {\n"
                + " imageArray[i] = new Image();\n"
                + " imageArray[i] = theSlides[i].image_path + theSlides[i].image_file_name;\n"
                + " theCaptions[i] = theSlides[i].slide_caption;\n"
                + " }\n"
                + " //Get the first slide(image) of the array and display it. \n"
                + " document.getElementById(\"firstImage\").src = imageArray[index];\n"
                + " document.getElementById(\"nav\").innerHTML = theCaptions;\n"
                + " });\n"
                + " });*/\n"
                + "\n"
                + "//imageArray[0] = new Image();\n"
                + "//imageArray[0].src = \"file:///C:/Users/WaleO/WorkSpace2/SlideshowMaker/sites/UtahTrip/img/SaltUtah.jpg\";\n"
                + "\n"
                + "//imageArray[1] = new Image();\n"
                + "//imageArray[1].src = \"file:///C:/Users/WaleO/WorkSpace2/SlideshowMaker/sites/UtahTrip/img/OregonTrailNebraska.jpg\";\n"
                + "\n"
                + "//imageArray[2] = new Image();\n"
                + "//imageArray[2].src = \"file:///C:/Users/WaleO/WorkSpace2/SlideshowMaker/sites/UtahTrip/img/QuirponIslandNewfoundland.jpg\";\n"
                + "\n"
                + "window.onload = function(){\n"
                + "    \n"
                + "    document.getElementById(\"firstImage\").src = slideShow[0];\n"
                + "};\n"
                + "\n"
                + "function next()\n"
                + "{\n"
                + "    if (index == (slideShow.length - 1))\n"
                + "    {\n"
                + "        index = 0;\n"
                + "        document.getElementById(\"firstImage\").src = slideShow[0];\n"
                + "        reload();\n"
                + "    }\n"
                + "    else\n"
                + "    {\n"
                + "        document.getElementById(\"firstImage\").src = slideShow[index + 1];\n"
                + "        index++;\n"
                + "        reload();\n"
                + "    }\n"
                + "}\n"
                + "\n"
                + "function previous()\n"
                + "{\n"
                + "        if (index == 0)\n"
                + "    {\n"
                + "        index = (slideShow.length - 1);\n"
                + "        document.getElementById(\"firstImage\").src = slideShow[index];\n"
                + "        reload();\n"
                + "    }\n"
                + "    else\n"
                + "    {\n"
                + "        document.getElementById(\"firstImage\").src = slideShow[index - 1];\n"
                + "        index--;\n"
                + "        reload();\n"
                + "    }\n"
                + "}\n"
                + "\n"
                + "function play()\n"
                + "{\n"
                + "\n"
                + "    if (clickCount)\n"
                + "    {\n"
                + "        timer = setInterval(\"next()\", interval);\n"
                + "        document.getElementById(\"playOrpauseButton\").src = \"file:///C:/Users/WaleO/WorkSpace2/SlideshowMaker/sites/UtahTrip/img/pause.png\";\n"
                + "        //The user has clicked the button once. \n"
                + "        clickCount = 0;\n"
                + "    }\n"
                + "    else\n"
                + "    {\n"
                + "        document.getElementById(\"playOrpauseButton\").src = \"file:///C:/Users/WaleO/WorkSpace2/SlideshowMaker/sites/UtahTrip/img/play-button-small.png\";\n"
                + "        clickCount = 1;\n"
                + "        clearInterval(timer);\n"
                + "    }\n"
                + "}\n"
                + "";

        String theJavaScript = javaScriptArrayName + jsonString;
        PrintWriter writer = null;

        try {

            // Create one directory of the slideShow under sites. 
            //First parameter is the parent directory, second parameter is the child directory. 
            boolean success = (new File(sitesDirectory, slideShowTitle)).mkdir();

            //This creates the CSS directory in the created SlideShow directory. 
            success = (new File(slideShowPath, cssDirectory)).mkdir();

            //This creates the JS directory in the created SlideShow directory. 
            success = (new File(slideShowPath, jsDirectory)).mkdir();

            //This creates the JS directory in the created SlideShow directory. 
            success = (new File(slideShowPath, imgDirectory)).mkdir();

            //aSlide.getImagePath and aSlide.getImageFileName.(source) (destination): getImageFileName;
            //These are the destinations for the copied html,css and javascript files.
            Files.copy(sourceHTMLFile.toPath(), destinationHTMLFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
            Files.copy(sourceCSSFile.toPath(), destinationCSSFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
            Files.copy(sourceJSFile.toPath(), destinationJSFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
            int i = 0;

            //This will iterate through the slideShow and get the slides and put them in the img directory. 
            for (Slide theSlide : ui.getSlideShow().getSlides()) {
                sourceImage[i] = new File(theSlide.getImagePath() + "/" + theSlide.getImageFileName());
                destinationImg[i] = new File(imgPath + "/" + theSlide.getImageFileName());
                Files.copy(sourceImage[i].toPath(), destinationImg[i].toPath(), StandardCopyOption.REPLACE_EXISTING);
                i++;
            }

            writer = new PrintWriter(destinationJSFile);
            writer.write(theJavaScript);

        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        } finally {
            writer.close();
        }
        System.out.println(slideShowPath);
        slideShowPath = slideShowPath.replaceAll("\\\\", "/");
        System.out.println(slideShowPath);
        //Following code creates the webview that has the title of the slideshow.
        WebView web = new WebView();
        web.getEngine().load("file:/" + destinationHTMLFile.getAbsolutePath());
        System.out.println(slideShowPath);
        Stage secondStage = new Stage();
        Scene secondScene = new Scene(web, 500, 500);
        secondStage.setTitle(ui.getSlideShow().getTitle());
        secondStage.setScene(secondScene);
        secondStage.show();
    }

    /**
     * This method will exit the application, making sure the user doesn't lose
     * any data first.
     */
    public void handleExitRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToExit = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE
                continueToExit = promptToSave();
            }

            // IF THE USER REALLY WANTS TO EXIT THE APP
            if (continueToExit) {
                // EXIT THE APPLICATION
                System.exit(0);
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(LanguagePropertyType.ERROR_PROPERTIES_FILE_LOADING, "Exiting Error", "File was not properly exited.");
        }
    }

    public void handleChangeTitleRequest() {
        //The following code will prompt the use to enter a title for the 
        //slide show. 
        TextInputDialog dialog = new TextInputDialog("Enter Title");
        dialog.setTitle("Set Title");
        dialog.setHeaderText("Title Option ");
        dialog.setContentText("Please enter title:"); // Where the user puts input

        //If the file is already saved the user wont need to enter a title again.
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            //Sets the title of the Slideshow.
            ui.getSlideShow().setTitle(result.get());

            //Will be used to mark the file as unsaved when the title is changed. 
            ui.updateToolbarControls(saved);
        }

    }

    /**
     * This helper method verifies that the user really wants to save their
     * unsaved work, which they might not want to do. Note that it could be used
     * in multiple contexts before doing other actions, like creating a new
     * pose, or opening another pose, or exiting. Note that the user will be
     * presented with 3 options: YES, NO, and CANCEL. YES means the user wants
     * to save their work and continue the other action (we return true to
     * denote this), NO means don't save the work but continue with the other
     * action (true is returned), CANCEL means don't save the work and don't
     * continue with the other action (false is retuned).
     *
     * @return true if the user presses the YES option to save, true if the user
     * presses the NO option to not save, false if the user presses the CANCEL
     * option to not continue.
     */
    private boolean promptToSave() throws IOException {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);

        //This code will bring up the pop up combo box for the language selection. 
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Save");
        alert.setHeaderText("Save Options Below");
        alert.setContentText("Do you want to save?");

        ButtonType buttonYes = new ButtonType("Yes");
        ButtonType buttonNo = new ButtonType("No");
        ButtonType buttonCancel = new ButtonType("Cancel");
        boolean saveWork = false;
        alert.getButtonTypes().setAll(buttonYes, buttonNo, buttonCancel);
        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == buttonYes) {
            saveWork = true;
        } else {
            if (result.get() == buttonCancel) {
                saveWork = false;
            }
        }

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        //If the user already hit the save button the work is saved
        //So there should be no need to be asked to save again.
        if (saved == false) {
            if (saveWork) {
                SlideShowModel slideShow = ui.getSlideShow();
                slideShowIO.saveSlideShow(slideShow);
                saved = true;
            } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
            // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
            else if (!true) {
                return false;
            }
        }

        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
    }

    /**
     * This helper method asks the user for a file to open. The user-selected
     * file is then loaded and the GUI updated. Note that if the user cancels
     * the open process, nothing is done. If an error occurs loading the file, a
     * message is displayed, but nothing changes.
     */
    private void promptToOpen() {
        // AND NOW ASK THE USER FOR THE COURSE TO OPEN
        FileChooser slideShowFileChooser = new FileChooser();
        slideShowFileChooser.setInitialDirectory(new File(PATH_SLIDE_SHOWS));
        File selectedFile = slideShowFileChooser.showOpenDialog(ui.getWindow());

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
                SlideShowModel slideShowToLoad = ui.getSlideShow();
                slideShowIO.loadSlideShow(slideShowToLoad, selectedFile.getAbsolutePath());
                ui.reloadSlideShowPane(slideShowToLoad);
                saved = true;
                ui.updateToolbarControls(saved);
            } catch (Exception e) {
                ErrorHandler eH = ui.getErrorHandler();
                eH.processError(TOOLTIP_NEXT_SLIDE, imagePath, imagePath);
            }
        }
    }

    /**
     * This mutator method marks the file as not saved, which means that when
     * the user wants to do a file-type operation, we should prompt the user to
     * save current work first. Note that this method should be called any time
     * the pose is changed in some way.
     */
    public void markFileAsNotSaved() {
        saved = false;
    }

    /**
     * Accessor method for checking to see if the current pose has been saved
     * since it was last editing. If the current file matches the pose data,
     * we'll return true, otherwise false.
     *
     * @return true if the current pose is saved to the file, false otherwise.
     */
    public boolean isSaved() {
        return saved;
    }

    //The following code will prompt the use to enter a title for the 
    //slide show. 
    /**
     * TextInputDialog dialog = new TextInputDialog("Enter Title");
     * dialog.setTitle("Set Title"); dialog.setHeaderText("Title Option ");
     * dialog.setContentText("Please enter title:"); // Where the user puts
     * input
     *
     *
     * Optional<String> result = dialog.showAndWait(); if (result.isPresent()){
     * //Sets the title of the Slideshow
     * ui.getSlideShow().setTitle(result.get()); }
     */
}
