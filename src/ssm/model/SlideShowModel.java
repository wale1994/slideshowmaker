package ssm.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import ssm.view.SlideEditView;
import ssm.view.SlideShowMakerView;

/**
 * This class manages all the data associated with a slideshow.
 * 
 * @author McKilla Gorilla & _____________
 * @co_author Olawale Onigemo
 */
public class SlideShowModel {
    SlideShowMakerView ui;
    String title;
    ObservableList<Slide> slides;
    Slide selectedSlide;
    String text;
    int count; //be used to keep track of number of slides. 
  
  
    public SlideShowModel(SlideShowMakerView initUI) {
	ui = initUI;
	slides = FXCollections.observableArrayList();
	reset();
    }

    // ACCESSOR METHODS
    public boolean isSlideSelected() {
	return selectedSlide != null;
        
    }
    
    public ObservableList<Slide> getSlides() {
	return slides;
    }
    
    public Slide getSelectedSlide() {
	return selectedSlide;
    }

    public String getTitle() { 
	return title; 
    }
    
    // MUTATOR METHODS
    public void setSelectedSlide(Slide initSelectedSlide) {
	selectedSlide = initSelectedSlide;
    }
    
    public void setTitle(String initTitle) { 
	title = initTitle; 
    }

    // SERVICE METHODS
    
    /**
     * Resets the slide show to have no slides and a default title.
     */
    public void reset() {
	slides.clear();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	title = props.getProperty(LanguagePropertyType.DEFAULT_SLIDE_SHOW_TITLE);
	selectedSlide = null;
    }
    
    /**
     * Every time a slide is created this method will count the number of slides
     * @return int - representing the number of slides
     */
    

    /**
     * Adds a slide to the slide show with the parameter settings.
     * @param initImageFileName File name of the slide image to add.
     * @param initImagePath File path for the slide image to add.
     */
    public void addSlide(   String initImageFileName,
			    String initImagePath, String caption) {
	Slide slideToAdd = new Slide(initImageFileName, initImagePath, caption);
	slides.add(slideToAdd);
	//selectedSlide = slideToAdd;
	ui.reloadSlideShowPane(this);
        count++; //adds to the number of slides. 
        
    }
    
    /**
     * This will remove the slide selected and the selected slide ONLY 
     * @param theSlide 
     */
    public void removeSlide(Slide theSlide)
    {
        Slide slideToDelete = theSlide;
        slides.remove(slideToDelete);
        selectedSlide = null;
        ui.reloadSlideShowPane(this);
        count--; 
    }
    
    /**
     * This will return the number of slides of the slide show.
     * @return int - represents the number of slides.
     */
    public int numberOfSlides()
    {
        return slides.size();
    }
    
   /**This will compare the slide being sent over to the slide already selected in
    * slideShow. 
    * @param aSlide
    * @return true if its a match, false if not.
    */
  
   public boolean testSlide(Slide aSlide)
   {
       if(selectedSlide.equals(aSlide))
           return true;
       
       return false;
       
   }
  
  
    public void upSlide(Slide theSlide)
    {
      int i = slides.indexOf(selectedSlide);
      selectedSlide = theSlide;
      
      //Change this to avoid null pointer.
      Slide dummySlide; 
      dummySlide = slides.get(i - 1);
      
      
      if(slides.indexOf(selectedSlide) >= 0)
      {
          slides.set((i-1), selectedSlide); 
          slides.set(i, dummySlide);          
      }
      ui.reloadSlideShowPane(this);
    }
  
    public void downSlide(Slide theSlide)
     {
      int i = slides.indexOf(selectedSlide);
      selectedSlide = theSlide;
      Slide dummySlide = slides.get(i + 1);
      if(i != slides.size())
      {
          slides.set((i+1), selectedSlide); 
          slides.set(i, dummySlide);          
      }
 
      ui.reloadSlideShowPane(this);
    }
}