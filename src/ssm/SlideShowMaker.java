package ssm;

import javafx.application.Application;
import javafx.stage.Stage;
import xml_utilities.InvalidXMLFileFormatException;
import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.TITLE_WINDOW;
import static ssm.StartupConstants.PATH_DATA;
import static ssm.StartupConstants.PROPERTIES_SCHEMA_FILE_NAME;
import static ssm.StartupConstants.UI_PROPERTIES_FILE_NAME;
import static ssm.StartupConstants.UI_PROPERTIES_FILE_SPANISH;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;
import ssm.view.SlideShowMakerView;
import java.util.*;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;
import javafx.scene.image.Image;
import javax.swing.JOptionPane;
import static ssm.StartupConstants.CSS_CLAS_LANGUAGE_SELECTION;
import static ssm.StartupConstants.ERROR_TITLESP;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import static ssm.StartupConstants.XML_ERRORSP;

/**
 * SlideShowMaker is a program for making custom image slideshows. It will allow
 * the user to name their slideshow, select images to use, select captions for
 * the images, and the order of appearance for slides.
 *
 * @author McKilla Gorilla & _____________
 */
public class SlideShowMaker extends Application {

    // THIS WILL PERFORM SLIDESHOW READING AND WRITING
    SlideShowFileManager fileManager = new SlideShowFileManager();
    String language = new String();

    // THIS HAS THE FULL USER INTERFACE AND ONCE IN EVENT
    // HANDLING MODE, BASICALLY IT BECOMES THE FOCAL
    // POINT, RUNNING THE UI AND EVERYTHING ELSE
    SlideShowMakerView ui = new SlideShowMakerView(fileManager);

    @Override
    public void start(Stage primaryStage) throws Exception {
        // LOAD APP SETTINGS INTO THE GUI AND START IT UP

        boolean success = loadProperties();
        if (success) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String appTitle = props.getProperty(TITLE_WINDOW);

            // NOW START THE UI IN EVENT HANDLING MODE
            ui.startUI(primaryStage, appTitle);
        } // THERE WAS A PROBLEM LOADING THE PROPERTIES FILE
        else {
            // LET THE ERROR HANDLER PROVIDE THE RESPONSE
            ErrorHandler errorHandler = ui.getErrorHandler();
            errorHandler.processError(LanguagePropertyType.ERROR_DATA_FILE_LOADING, "Loading Error", "File Could not be loaded");
            System.exit(0);
        }

    }

    /**
     * Loads this application's properties file, which has a number of settings
     * for initializing the user interface.
     *
     * @return true if the properties file was loaded successfully, false
     * otherwise.
     */
    public boolean loadProperties() {
        try {
            // LOAD THE SETTINGS FOR STARTING THE APP
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);

            //This code will bring up the pop up combo box for the language selection. 
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Language Selection");
            alert.setHeaderText("Language Options Below ");
            alert.setContentText("Choose your language.");

            DialogPane dialogPane = alert.getDialogPane();
            
            //Adds the style sheet necessary to apply css to the regions of the 
            //dialog box.
            dialogPane.getStylesheets().add(STYLE_SHEET_UI);
            dialogPane.getStyleClass().add(CSS_CLAS_LANGUAGE_SELECTION);
            

            ButtonType buttonTypeOne = new ButtonType("English");
            ButtonType buttonTypeTwo = new ButtonType("Spanish");
            
            alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo);
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == buttonTypeOne) {
                //if buttonTypeOne (English) is chosen then english slide show will pop up
                //Set the variable to english to make other text specified in english.
                props.loadProperties(UI_PROPERTIES_FILE_NAME, PROPERTIES_SCHEMA_FILE_NAME);
                language = new String("English");
            } else if (result.get() == buttonTypeTwo) {
                //if ButtonTypeTwo is chosen (Spanish) the text of the slide show will be in spanish.
                //Set language to spanish to cause the program to run other text including errors in spanish.
                props.loadProperties(UI_PROPERTIES_FILE_SPANISH, PROPERTIES_SCHEMA_FILE_NAME);
                language = new String("Spanish");
            }
            return true;

        } catch (InvalidXMLFileFormatException ixmlffe) {
            // SOMETHING WENT WRONG INITIALIZING THE XML FILE
            ErrorHandler eH = ui.getErrorHandler();
            if (language.equals(new String("English"))) {
                eH.processError(LanguagePropertyType.ERROR_PROPERTIES_FILE_LOADING, "Slide Show Error", "XML File could not be initialized");
            } else {
                eH.processError(LanguagePropertyType.ERROR_PROPERTIES_FILE_LOADING, ERROR_TITLESP, XML_ERRORSP);
            }

            return false;
        }
    }

    /**
     * This is where the application starts execution. We'll load the
     * application properties and then use them to build our user interface and
     * start the window in event handling mode. Once in that mode, all code
     * execution will happen in response to user requests.
     *
     * @param args This application does not use any command line arguments.
     */
    public static void main(String[] args) {
        launch(args);
    }
}
